use std::process::{Command, Stdio};
use std::io::Read;
use std::string::String;

mod condensable;
use condensable::Condensable;

// reference: https://android.googlesource.com/platform/system/core/+/master/liblog/include/log/{log.h,log_id.h,log_read.h}

// len = payload (msg) length
// hdr_size = TOTAL size of header, including length values

// https://android.stackexchange.com/questions/30207/what-is-the-format-of-androids-logs
// For main, radio and system logs msg field is interpreted as follows (source):
//
//     priority: 1 byte
//     tag: 0 or more bytes
//     literal \0 as separator
//     message: 0 or more bytes
//     literal \0 as terminator
//
// If this message is truncated, the trailing \0 may be missing.
//
// For events log however, msg field contain the following binary data:
//
//     Tag: 4 bytes integer key from "/system/etc/event-log-tags" file.
//     Message: serialized tree starting from the root node. Each tree node starts with a 1-byte value from AndroidEventLogType enumeration:
//         EVENT_TYPE_INT - node value is 4 bytes integer
//         EVENT_TYPE_LONG - node value is 8 bytes integer
//         EVENT_TYPE_STRING - node value is 4 bytes integer length, followed by the length bytes containing a UTF8-encoded string
//         EVENT_TYPE_LIST - node value is a single-byte length, followed by the length tree nodes each of its own AndroidEventLogType.


#[derive(Debug)]
enum LogEntryVersion {
    V1,
    V3,
    V4,
    Unknown
}

#[derive(Debug)]
struct LogEntry {
    version: LogEntryVersion,
    pid: i32,
    tid: i32,
    time: i32,
    nanotime: i32,
    uid: u32,
    lid: LogID,
    payload: LogPayload
}

impl LogEntry {
    fn from_raw(h: &[u8], b: &[u8]) -> Option<LogEntry> {
        Some(LogEntry {
            version: if h.len() == 20 {
                LogEntryVersion::V1
            } else if h.len() == 24 {
                LogEntryVersion::V3
            } else if h.len() == 28 {
                LogEntryVersion::V4
            } else {
                LogEntryVersion::Unknown
            },

            pid: Condensable::from_slice(&h[0..4]),
            tid: Condensable::from_slice(&h[4..8]),
            time: Condensable::from_slice(&h[8..12]),
            nanotime: Condensable::from_slice(&h[12..16]),

            lid: if h.len() >= 24 {
                let temp: u32 = Condensable::from_slice(&h[16..20]);
                LogID::from_u32(temp)
            } else {
                LogID::Unknown
            },

            uid: if h.len() == 28 {
                Condensable::from_slice(&h[20..24])
            } else {
                0
            },

            payload: LogPayload::new()
        }).map(|mut x| {
            if x.lid == LogID::Events {
                x.payload = LogPayload::deserialize_events(b).unwrap_or(LogPayload::new())
            } else {
                x.payload = LogPayload::deserialize(b).unwrap_or(LogPayload::new())
            }

            x
        })
    }
}

#[derive(Debug, PartialEq)]
enum LogID {
    Main,
    Radio,
    Events,
    System,
    Crash,
    Security,
    Kernel,
    Unknown
}

impl LogID {
    fn from_u32(x: u32) -> LogID {
        match x {
            0 => LogID::Main,
            1 => LogID::Radio,
            2 => LogID::Events,
            3 => LogID::System,
            4 => LogID::Crash,
            5 => LogID::Security,
            6 => LogID::Kernel,
            _ => LogID::Unknown
        }
    }
}

#[derive(Debug)]
enum LogPriority {
    Verbose,
    Debug,
    Info,
    Warn,
    Error,
    Unknown
}

impl LogPriority {
    fn from_u8(x: u8) -> LogPriority {
        match x {
            1 => LogPriority::Verbose,
            2 => LogPriority::Debug,
            3 => LogPriority::Info,
            4 => LogPriority::Warn,
            5 => LogPriority::Error,
            _ => LogPriority::Unknown

        }
    }
}

#[derive(Debug)]
struct LogPayload {
    priority: LogPriority,
    tag: String,
    message: String
}

impl LogPayload {
    fn new() -> Self {
        LogPayload {
            priority: LogPriority::Unknown,
            tag: String::from(""),
            message: String::from("")
        }
    }

    // message:
    // <priority: u8> <tag: string> \0 <message: String> \0
    fn deserialize(x: &[u8]) -> Option<Self> {
        let mut first_null: usize = 0;
        let mut second_null: usize = 0;

        for (i, v) in x.iter().enumerate() {
            if *v == 0 {
                if first_null == 0 {
                    first_null = i;
                } else if second_null == 0 {
                    second_null = i;
                    break;
                }
            }
        };

        if first_null == 0 || second_null == 0 {
            return None
        }
        
        Some(
            LogPayload {
                priority: LogPriority::from_u8(x[0]),
                tag: String::from_utf8_lossy(&x[1..first_null]).to_string(),
                message: String::from_utf8_lossy(&x[first_null+1..second_null]).to_string()
            }
        )
    }

    fn deserialize_events(x: &[u8]) -> Option<Self> {
        Some(LogPayload::new())
    }
}

struct Logcat<'a> {
    source: &'a mut dyn Read
}

impl<'a> Logcat<'a> {
    fn new(input: &'a mut impl Read) -> Logcat {
        Logcat {
            source: input
        }
    }
}
    

impl<'a> Iterator for Logcat<'a> {
    type Item = LogEntry;

    fn next(&mut self) -> Option<Self::Item> {
        let mut raw_payload_len: Vec<u8> = vec![0; 2];

        if let Err(_) = self.source.read_exact(&mut raw_payload_len) {
            return None
        }
        
        let payload_len = u16::from_be(
            ((raw_payload_len[0] as u16) << 8) | raw_payload_len[1] as u16
        );

        let mut raw_header_len: Vec<u8> = vec![0; 2];
        if let Err(_) = self.source.read_exact(&mut raw_header_len) {
            return None
        }
        
        let header_len = u16::from_be(
            ((raw_header_len[0] as u16) << 8) | raw_header_len[1] as u16
        );

        // header_len - 4 since payload and header length are each 2 bytes
        let mut raw_header: Vec<u8> = vec![0; (header_len - 4) as usize];
        if let Err(_) = self.source.read_exact(&mut raw_header) {
            return None;
        }

        let mut raw_entry: Vec<u8> = vec![0; payload_len as usize];
        if let Err(_) = self.source.read_exact(&mut raw_entry) {
            return None;
        }

        Some(LogEntry::from_raw(&raw_header, &raw_entry).unwrap())
    }
}


fn main() -> Result<(), std::io::Error> {
    let mut is_live = true;
    let mut filename = String::from("");

    let mut args = std::env::args();
    args.next(); // skip binary name

    if let Some(x) = args.next() {
        if x == "--file" {
            is_live = false;

            if let Some(f) = args.next() {
                filename = f;
            }
        }
    }

    if is_live {
        let mut logcat: std::process::Child = Command::new("adb")
            .arg("logcat")
            .arg("-B")
            .stdout(Stdio::piped())
            .spawn()?;

        if let Some(x) = logcat.stdout.as_mut() {
            Logcat::new(x)
                .filter(|x| x.lid == LogID::Events)
                .for_each(|x| println!("{:#?}: {:#?}", x.payload.tag, x.payload.message));
        }
        
        match logcat.kill() {
            Ok(()) => println!("Killed child process"),
            Err(_) => println!("Unable to kill child process")
        }
    } else {
        Logcat::new(&mut std::fs::File::open(filename)?)
            .filter(|x| x.lid == LogID::System)
            .for_each(|x| println!("{:#?}: {:#?}", x.lid, x.payload.tag));
    }

    Ok(())
}
