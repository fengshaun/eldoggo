pub trait Condensable {
    fn from_slice(x: &[u8]) -> Self;
}

impl Condensable for i16 {
    fn from_slice(x: &[u8]) -> i16 {
        i16::from_be(((x[2] as i16) << 8) | ((x[3] as i16)))
    }
}

impl Condensable for i32 {
    fn from_slice(x: &[u8]) -> i32 {
        i32::from_be(((x[0] as i32) << 24) |
                     ((x[1] as i32) << 16) |
                     ((x[2] as i32) << 8)  |
                     ((x[3] as i32) << 0))
    }
}
    
impl Condensable for u16 {
    fn from_slice(x: &[u8]) -> u16 {
        u16::from_be(((x[2] as u16) << 8) | ((x[3] as u16)))
    }
}

impl Condensable for u32 {
    fn from_slice(x: &[u8]) -> u32 {
        u32::from_be(((x[0] as u32) << 24) |
                     ((x[1] as u32) << 16) |
                     ((x[2] as u32) << 8)  |
                     ((x[3] as u32) << 0))
    }
}
